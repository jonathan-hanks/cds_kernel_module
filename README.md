Investigations on converting the CDS realtime patch into a kernel module.

To build the test real time module
<pre>make</pre>

To build a test module
<pre>cd test_mod; KBUILD_EXTRA_SYMBOLS=`pwd`/../Module.symvers make</pre>

The cmake files in this repo are not used at this time.