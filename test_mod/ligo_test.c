#include <linux/init.h>
#include <linux/module.h>
#include <linux/cpu.h>
#include <linux/delay.h>

MODULE_LICENSE("Dual BSD/GPL");

#define CPU_ID 7

//extern int cpu_up( unsigned int cpu );
//extern int cpu_down( unsigned int cpu );
extern void set_fe_code_idle(void (*ptr)(void), unsigned int cpu);

static atomic_t module_done = ATOMIC_INIT(0);
static atomic_t idle_loop_called = ATOMIC_INIT(0);

static void idle_loop(void)
{
    atomic_add(1, &idle_loop_called);
//    while (atomic_read(&module_done) == 0)
//    {
//
//    }
//    atomic_set(&module_done, 42);
}


static int hello_init(void)
{
    set_fe_code_idle(idle_loop, CPU_ID);
    atomic_set(&module_done, 0);
    //cpu_down(CPU_ID);
    remove_cpu(CPU_ID);
    return 0;
}

static void hello_exit(void)
{
    set_fe_code_idle( NULL, CPU_ID );
    atomic_set(&module_done, 1);
    atomic_set(&module_done, 42);
    printk(KERN_ALERT "ligo idle loop called %d times", (int)atomic_read(&idle_loop_called));
    add_cpu(CPU_ID);
    //cpu_up(CPU_ID);
    while (atomic_read(&module_done) != 42)
    {
        usleep_range(1000000, 1000000+100);
    }

}

module_init(hello_init);
module_exit(hello_exit);