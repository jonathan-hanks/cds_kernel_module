#include <linux/init.h>
#include <linux/module.h>
#include <linux/cpu.h>

MODULE_LICENSE("Dual BSD/GPL");


static void (*original_play_dead_handler)(void) = NULL;

static DEFINE_PER_CPU(void (*)(void), fe_code);

int is_cpu_taken_by_rcg_model(unsigned int cpu)
{
    return 0 != per_cpu(fe_code, cpu);
}
EXPORT_SYMBOL(is_cpu_taken_by_rcg_model);

void set_fe_code_idle(void (*ptr)(void), unsigned int cpu)
{
    per_cpu(fe_code, cpu) = ptr;
}
EXPORT_SYMBOL(set_fe_code_idle);


static void ligo_play_dead(void)
{
    void (*handler)(void) = per_cpu(fe_code, smp_processor_id());
    printk(KERN_ALERT "entering ligo_play_dead, cpu handler = %p", handler);

    if (handler)
    {
        printk(KERN_ALERT "calling LIGO code");
        local_irq_disable();
        (*handler)();
        printk(KERN_ALERT "LIGO code is done, calling regular shutdown code");
        (*original_play_dead_handler)();
    }
    else
    {
        (*original_play_dead_handler)();
    }
}


static int ligo_rt_init(void)
{
    original_play_dead_handler = smp_ops.play_dead;
    printk(KERN_ALERT "ligo_rt_init.  Handler was %p\n", smp_ops.play_dead);
    smp_ops.play_dead = ligo_play_dead;
    printk(KERN_ALERT "Handler is now %p", smp_ops.play_dead);
    return 0;
}

static void ligo_rt_exit(void)
{
    smp_ops.play_dead = original_play_dead_handler;
    printk(KERN_ALERT "ligo_rt_exit.  Handler restored to %p\n", smp_ops.play_dead);
}

module_init(ligo_rt_init);
module_exit(ligo_rt_exit);

